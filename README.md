This xnat-clara-train-sdk image is an XNAT enabled wrapper for the [Nvidia Clara Train SDK v2.0 container](nvcr.io/nvidia/clara-train-sdk:v2.0)

Processing scripts are derived from NVIDIA's MMAR command examples (e.g. [CT Spleen Segmentation](https://ngc.nvidia.com/catalog/models/nvidia:med:segmentation_ct_spleen))

The included MMAR processing scripts and enviroment file expect data and configuration to be mounted as:


* train.sh 
	/workspace/data/ - dataset root referenced by pathes in dataset.json. e.g. project archive root.
	/workspace/config/ - path contains a single config*.json model file, e.g. config_train.json.
	/workspace/config/dataset.json - file contains partitioned (train, validation, test) paths to image/mask data, relative to /workspace/data/ mount.
	/output - empty directory set as output of processing.
	
* train_finetune.sh config_file_name.json model_file_name.ckpt [additional_options]
	/workspace/data/ - dataset root referenced by pathes in dataset.json. e.g. project archive root.
	/workspace/config/ - path contains a single config*.json model file, e.g. config_train.json.
	/workspace/config/dataset.json - file contains partitioned (train, validation, test) paths to image/mask data, relative to /workspace/data/ mount.
	/workspace/model/ - path contains trained ckpt files.
	/output - empty directory set as output of processing.
 
 Run image as:

 docker run -it --rm --ipc=host --net=host --runtime=nvidia \
 		-v /path-to-project-archive/:/workspace/data/ \
 		-v  /path-to--folder containing-dataset-json-file:/dataset \
 		-v /path-to-host-output-dir/:/output \
 				xnat/xnat-clara-train:fake
 
 
 
* infer.sh
	/workspace/config/ - path contains a single config*.json model file, e.g. config_validation.json.
	/workspace/model/ - path contains trained ckpt or *.pb model files.
	/output - empty directory set as output of processing.
 
* export.sh
	/workspace/model/ - path contains trained ckpt files.
	/output - empty directory set as output of processing.





Example dataset.json

`
{
    "description": "Spleen Segmentation",
    "labels": {
        "0": "background",
        "1": "spleen"
    },
    "licence": "CC-BY-SA 4.0",
    "modality": {
        "0": "CT"
    },
    "name": "Spleen",
    "numTest": 20,
    "numTraining": 41,
    "reference": "Memorial Sloan Kettering Cancer Center",
    "release": "1.0 06/08/2018",
    "tensorImageSize": "3D",
    "training": [
        {
            "image": "imagesTr/spleen_29.nii",
            "label": "labelsTr/spleen_29.nii"
        },
        {
            "image": "imagesTr/spleen_46.nii",
            "label": "labelsTr/spleen_46.nii"
        },
        {
            "image": "imagesTr/spleen_25.nii",
            "label": "labelsTr/spleen_25.nii"
        },
        {
            "image": "imagesTr/spleen_13.nii",
            "label": "labelsTr/spleen_13.nii"
        },
		...
`