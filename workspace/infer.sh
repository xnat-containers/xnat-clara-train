#!/usr/bin/env bash
die(){
    echo >&2 "$@"
    exit 1
}

echo $@
echo "Usage: infer.sh config_file_name.json model_file_name.ckpt [additional_options]"
echo "Config: /workspace/config/config_file_name.json
echo "DataRoot: /workspace/data"
echo "DatasetJson: /workspace/config/dataset.json"
echo "ProcessingTask: segmentation"
echo "MMAR_CKPT: /workspace/model/model_file_name.ckpt"
echo "Output: /output"

CONFIG_FILE=$1
shift
MODEL_FILE=$1
shift
ADDITIONAL_OPTIONS="$@"

my_dir="$(dirname "$0")"
. $my_dir/set_env.sh
echo "MMAR_ROOT set to $MMAR_ROOT"
echo "CONFIG_FILE set to $CONFIG_FILE"
echo "MODEL_FILE set to $MODEL_FILE"
echo "ADDITIONAL_OPTIONS set to $ADDITIONAL_OPTIONS"


python3 -u  -m nvmidl.apps.evaluate \
    -m $MMAR_ROOT \
    -c $CONFIG_FILE \
    --set \
    DATA_ROOT="/workspace/data" \
    DATASET_JSON=/workspace/config/dataset.json \
    PROCESSING_TASK="segmentation" \
    MMAR_CKPT=/workspace/model/$MODEL_FILE \
    MMAR_CKPT_DIR="/output" \
    MMAR_EVAL_OUTPUT_PATH="/output" \ 
    output_infer_result=true \
    do_validation=false  ${additional_options}
	


