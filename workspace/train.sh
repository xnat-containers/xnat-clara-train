#!/usr/bin/env bash
die(){
    echo >&2 "$@"
    exit 1
}

echo $@
echo "Usage: train.sh config_file_name.json [additional_options]"

my_dir="$(dirname "$0")"
. $my_dir/set_env.sh

echo "Starting TensorBoard in background."
tensorboard --logdir=/output --host 0.0.0.0 --path_prefix /training/${TRAEFIK_ID} &

CONFIG_FILE=$MMAR_ROOT/config/$1
shift
ADDITIONAL_OPTIONS="$@"

echo "MMAR_ROOT set to $MMAR_ROOT"
echo "CONFIG_FILE set to $CONFIG_FILE"
echo "ADDITIONAL_OPTIONS set to $ADDITIONAL_OPTIONS"

export TF_ENABLE_AUTO_MIXED_PRECISION=0

## We may need to remove the archive path prefix from dataset.json
cp /workspace/config/dataset.json /tmp/
sed -i 's+/data/xnat/archive/[^"]*/arc001/+./+g' /tmp/dataset.json

python3 -u  -m nvmidl.apps.train \
    -m $MMAR_ROOT \
    -c $CONFIG_FILE \
    --set \
     DATA_ROOT="/workspace/data" \
     DATASET_JSON="/tmp/dataset.json" \
     PROCESSING_TASK="segmentation" \
     MMAR_CKPT_DIR="/output" \
     MMAR_EVAL_OUTPUT_PATH="/output" \ 
     PRETRAIN_WEIGHTS_FILE="/var/tmp/resnet50_weights_tf_dim_ordering_tf_kernels.h5" \
     multi_gpu=false \
     use_amp=false  ${additional_options}
